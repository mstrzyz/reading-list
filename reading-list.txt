GOOD DESIGN PRACTICES:
1. (!) Clean Code: A Handbook of Agile Software Craftsmanship (Robert C. Martin)
2. (!) The Clean Coder: A Code of Conduct for Professional Programmers (Robert C. Martin)
3. (!) The Pragmatic Programmer (Andrew Hunt)
4. (!) Software Estimation: Demystifying the Black Art (Developer Best Practices) (Steve McConnell)
5. Code Complete (Developer Best Practices) (Steve McConnell)
6. (!) Head First Design Patterns (Eric Freeman)
7. Design patterns : elements of reusable object-oriented software (Erich Gamma)
8. Patterns of Enterprise Application Architecture (The Addison-Wesley Signature Series) (Martin Fowler)
9. Enterprise Integration Patterns: Designing, Building, and Deploying Messaging Solutions (Addison-Wesley Signature)  (Gregor Hohpe)
10. Building Microservices (Sam Newman)
11. (!) Growing Object-Oriented Software, Guided by Tests (Beck Signature) (Steve Freeman)
12. The Agile Samurai: How Agile Masters Deliver Great Software (Pragmatic Programmers) (Jonathan Rasmusson)
13. (!) Effective Java: Third Edition (Joshua Bloch)
14. Applying UML and Patterns: An Introduction to Object-Oriented Analysis and Design and Iterative Development (Craig Larman)

TIME MANAGEMENT / ATTITUDE:
1. (!) Black Box Thinking: The Surprising Truth About Success (Matthew Syed)
2. (!) Bounce: The Myth of Talent and the Power of Practice
3. (!) The 7 Habits of Highly Effective People (Stephen R. Covey)

INTERNET/NEWS:
https://www.baeldung.com/java-web-weekly newsletter
Java magazine
Twitter (@martinfowler, @unclebobmartin, @KentBeck, @nicolas_frankel, @thjanssen123, @AdamBien, ‏@java )

PLANNING TO READ:
1. Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation (Addison-Wesley Signature) (Jez Humble)
2. Domain-Driven Design: Tackling Complexity in the Heart of Software (Eric Evans)
3. Release It! Design and Deploy Production-Ready Software (Michael T Nygard)
4. Continuous Integration: Improving Software Quality and Reducing Risk (Martin Fowler Signature Books) (Paul M. Duvall)
5. Accelerate: The Science of Lean Software and Devops: Building and Scaling High Performing Technology Organizations (Nicole Forsgren, Jez Humble)
6. Refactoring: Improving the Design of Existing Code (Addison-Wesley Object Technology)
7. Test Driven Development: By Example (The Addison-Wesley Signature Series) (Kent Beck)
8. Java Performance: The Definitive Guide (Scott Oaks)
9. Working Effectively with Legacy Code (Michael Feathers)
10. Clean Architecture: A Craftsman's Guide to Software Structure and Design (Robert C. Martin Series)  (Robert C. Martin)

OTHER:
Coursera